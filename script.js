const daysE1=document.getElementById('days');
const hoursE1=document.getElementById('hours');
const minutesE1=document.getElementById('minutes');
const secondsE1=document.getElementById('seconds');

const newYear= "1 Jan 2022";

function countdown(){
    const newYearDate=new Date(newYear);
    

    const currentDate=new Date();
    const totalSeconds= (newYearDate-currentDate)/1000;
 
    const days=Math.floor(totalSeconds / (3600*24*1000)*1000);
    const hours=Math.floor(totalSeconds /3600 %24);
    const minutes=Math.floor(totalSeconds /60 %60);
    const seconds=Math.floor(totalSeconds %60);
    //console.log(seconds);



daysE1.innerHTML=formatTime(days);
hoursE1.innerHTML=formatTime(hours);
minutesE1.innerHTML=formatTime(minutes);
secondsE1.innerHTML=formatTime(seconds);
}

function formatTime(time){
return time<10? '0${time}':time;
}

countdown();//Initial call
setInterval(countdown,1000);
//9945026886